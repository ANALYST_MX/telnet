#pragma once

typedef enum {
  EX_INVALID_RESOURCE_FORMAT,
  EX_STEAM_CONDENSER,
  EX_CONNECT_INVOKE,
  EX_SOCKET_ERROR
} ex_status_t;
