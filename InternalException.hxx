#pragma once

#include <exception>
#include <string>

#include "ExTypes.hxx"

class InternalException : public std::exception
{
public:
  InternalException(const std::string &method, const std::string &message,
                    ex_status_t status) throw()
    : std::exception(),
      throwingMethod(method), mesg(message), statusCode(status)
  {}

  ~InternalException() throw() {}

  virtual const char * what() const throw() { return "InternalException"; }

  const char * getThrowingMethod() const { return throwingMethod.c_str(); }

  const char * getMessage() const { return mesg.c_str(); }

  ex_status_t getStatusCode() const { return statusCode; }

private:
  std::string throwingMethod;
  std::string mesg;
  ex_status_t statusCode;
};
