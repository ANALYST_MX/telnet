QT += core
QT -= gui

TARGET = Telnet
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cxx \
    Connection.cxx \
    Port.cxx

HEADERS += \
    main.hxx \
    Connection.hxx \
    Port.hxx \
    InternalException.hxx \
    ExTypes.hxx

CONFIG += c++11

LIBS += -lws2_32
