#include "Port.hxx"

Port::Port(int portNumber)
{
  const std::string func = "Port::Port(int)";
  if(!isValidPort(portNumber))
    {
      throw InternalException(func, "Invalid port number please choose a port between 1 and 65535", EX_INVALID_RESOURCE_FORMAT);
    }

  this->portNumber = portNumber;
}

bool Port::isValidPort(int portNumber)
{
  if(portNumber >= 0 || portNumber <= 65535)
    {
      return true;
    }
  return false;
}

int getPort()
{
  return getPort();
}

