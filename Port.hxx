#pragma once

#include <string>

#include "InternalException.hxx"
#include "ExTypes.hxx"

class Port
{
public:
  Port(int);
  int getPort();
  static bool isValidPort(int);
private:
  int portNumber;
};
