#pragma once

#include <winsock2.h>
#include <string>
#include <iostream>

#include "InternalException.hxx"
#include "ExTypes.hxx"

class Connection
{
public:
  Connection(int = 23);
  virtual ~Connection();

  bool open(char *);
  bool sendData(char *);
  char * receive();
  //bool close(void);
  // FIXME: add function for closing connection

private:
  int port;
  struct sockaddr_in dest;
  SOCKET s;
};
