#include "Connection.hxx"

Connection::Connection(int port)
  : port(port)
{
  /* zero the struct */
  memset(&dest, 0, sizeof(dest));
  /* Create a stream TCP socket. */
  s = socket(AF_INET, SOCK_STREAM, 0);
}

Connection::~Connection()
{
  closesocket(s);
}

bool Connection::open(char * addr)
{
  const std::string func = "Connection::open(char *)";
  try
  {
    // get the addresses
    hostent * host = gethostbyname(addr);

    // Not a valid URL
    if(NULL == host)
      {
        // See if we have a vaild IP address
        host = gethostbyaddr(addr, strlen(addr), AF_INET);

        // Still not valid
        if(NULL == host)
          {
            throw InternalException(func, "Cannot resolve ", EX_STEAM_CONDENSER);
          }
      }

    // Set up connection
    memcpy(&dest.sin_addr, host->h_addr, host->h_length);
    dest.sin_family = host->h_addrtype;
    dest.sin_port = htons(port);

    // Establish connection
    int connectionStatus = connect(s, (struct sockaddr *)&dest, sizeof addr);
    if (-1 == connectionStatus == 0)
      {
        throw InternalException(func, "Connect failed", EX_CONNECT_INVOKE);
      }
    return true;
  }
  catch (InternalException ex)
  {
    std::cerr << ex.getMessage() << std::endl;
  }

  return false;
}

bool Connection::sendData(char * data)
{
  const std::string func = "Connection::sendData(char *)";
  int result = send(s, data, strlen(data), NULL);
  if(SOCKET_ERROR == result)
    {
      throw InternalException(func, "Unable to send data", EX_SOCKET_ERROR);
    }

  return true;
}

char * Connection::receive()
{
  const std::string func = "Connection::receive()";
  int bytesReceived = 0;
  const int bufsize=1024 * 4; /* a 4K buffer */
  char * buffer = new char[bufsize];

  bytesReceived = recv(s, buffer, 1024 * 4,0);

  if(SOCKET_ERROR == bytesReceived)
    {
      throw InternalException(func, "Unable to receive data", EX_SOCKET_ERROR);
    }

  buffer[bytesReceived] = '\0';

  char *receivedData = new char[bytesReceived];
  strcpy(receivedData, buffer);
  delete[] buffer;

  return receivedData;
}

